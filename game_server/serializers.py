from rest_framework import serializers

from .models import GameSession


class GameConnectSerializer(serializers.Serializer):
    player_name = serializers.CharField()
    game_id = serializers.UUIDField(allow_null=True, required=False)


class GameDisconnectSerializer(serializers.Serializer):
    player_id = serializers.IntegerField()
    game_id = serializers.UUIDField()


class GameMoveSerializer(serializers.Serializer):
    player_id = serializers.IntegerField()
    game_id = serializers.UUIDField()
    column = serializers.IntegerField()


class GameSerializer(serializers.ModelSerializer):
    player_a = serializers.PrimaryKeyRelatedField(read_only=True)
    player_b = serializers.PrimaryKeyRelatedField(read_only=True)
    winner = serializers.PrimaryKeyRelatedField(read_only=True)
    current_turn = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = GameSession
        fields = ['id', 'board', 'player_a', 'player_b', 'winner', 'current_turn']
