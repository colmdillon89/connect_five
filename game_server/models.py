from django.db import models

import json
from uuid import uuid4
from django.db import models
from django.core import serializers
# Create your models here.

HEIGHT = 6
WIDTH = 9
WIN_CONNECT = 5


def _initialise_board():

    return [list([] for _ in range(0, WIDTH)) for _ in range(0, HEIGHT)]


class Player(models.Model):

    id = models.AutoField(
        primary_key=True,
    )

    name = models.CharField(
        max_length=128
    )

    # color = models.BinaryField()  # Black or white ...

    class Meta:
        app_label = 'game_server'


class GameSession(models.Model):

    id = models.UUIDField(
        primary_key=True,
        default=uuid4,
        editable=False
    )

    board = models.TextField(
        default=json.dumps(_initialise_board())
    )

    player_a = models.ForeignKey(
        Player,
        on_delete=models.CASCADE,
        related_name='player_a'
    )

    player_b = models.ForeignKey(
        Player,
        on_delete=models.CASCADE,
        null=True,
        default=None,
        related_name='player_b'
    )

    winner = models.ForeignKey(
        Player,
        on_delete=models.CASCADE,
        null=True,
        default=None,
        related_name='winner'
    )

    current_turn = models.ForeignKey(
        Player,
        null=True,
        default=None,
        on_delete=models.CASCADE,
        related_name='current_turn'
    )

    class Meta:
        app_label = 'game_server'
