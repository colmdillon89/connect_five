from django.shortcuts import render
from django.views.generic import DetailView
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import GameConnectSerializer, GameDisconnectSerializer, GameMoveSerializer, GameSerializer
from .functions import find_setup_game, disconnect_game, game_move
from .models import GameSession


class GameSessionMoveView(APIView):

    def post(self, request, *args, **kwargs):
        """
        Player move on the board.
        """
        move_serializer = GameMoveSerializer(data=request.data)

        if not move_serializer.is_valid():
            return Response(
                status=400,
                data={
                    "message": "Bad request data: {}".format(request.data)
                }
            )

        valid_data = move_serializer.validated_data

        game_data = game_move(**valid_data)

        return Response(
            status=201,
            data=game_data
        )


class GameSessionGetView(APIView):

    # queryset = GameSession.objects.all()

    # def get_object(self):
    #     """
    #     :return:
    #     """
    #     obj = super().get_object()
    #     return obj

    def get(self, request, *args, **kwargs):
        game_id = self.kwargs["game_id"]
        obj = GameSession.objects.filter(id=game_id).all()
        if len(obj) == 1:
            game_data = GameSerializer(obj[0]).data
            return Response(
                status=200,
                data=game_data
            )
        else:
            return Response(
                status=404,
            )


class GameSessionConnectView(APIView):

    def post(self, request, *args, **kwargs):
        """
        Create new game with the new user details.
        :return:
        """
        game_serializer = GameConnectSerializer(data=request.data)

        if not game_serializer.is_valid():
            return Response(
                status=400,
                data={
                    "message": "Bad request data: {}".format(request.data)
                }
            )

        valid_data = game_serializer.validated_data

        player_name = valid_data["player_name"]

        game_data = find_setup_game(player_name)

        return Response(
            status=201,
            data=game_data
        )


class GameSessionDisconnectView(APIView):

    def post(self, request, *args, **kwargs):
        """
        Disconnect from game.
        :return:
        """

        game_serializer = GameDisconnectSerializer(data=request.data)

        if not game_serializer.is_valid():
            return Response(
                status=400,
                data={
                    "message": "Bad request data: {}".format(request.data)
                }
            )

        valid_data = game_serializer.validated_data

        player_id = valid_data["player_id"]
        game_id = valid_data["game_id"]

        game_data = disconnect_game(player_id, game_id)

        return Response(
            status=201,
            data=game_data
        )
