from django.urls import path

from . import views

urlpatterns = [
    path('connect', views.GameSessionConnectView.as_view()),
    path('disconnect', views.GameSessionDisconnectView.as_view()),
    path('move', views.GameSessionMoveView.as_view()),
    path('game/<str:game_id>/', views.GameSessionGetView.as_view())
]
