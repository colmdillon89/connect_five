import json
from django.test import TestCase, Client

# Create your tests here.

from .models import GameSession, Player


def set_up_test_game(num_players=2):
    player_a = Player.objects.create(name="Paul")
    player_b = Player.objects.create(name="Jim") if num_players > 1 else None

    game = GameSession.objects.create(
        player_a=player_a,
        player_b=player_b
    )

    return game


class GameClientTestCase(TestCase):

    def test_new_player_joins_a_game_as_first_player(self):

        player_one_c = Client()

        data = {
            "player_name": "Joe"
        }

        resp = player_one_c.post('/connect_five/connect', data=data)

        self.assertEqual(resp.status_code, 201)

    def test_new_player_joins_a_game_as_second_player(self):

        game = set_up_test_game(num_players=1)

        current_game_id = str(game.id)

        player_two_c = Client()

        data = {
            "player_name": "Paul"
        }

        resp = player_two_c.post('/connect_five/connect', data=data)

        self.assertEqual(resp.status_code, 201)

        self.assertEqual(resp.json()["game"]["id"], current_game_id)

    def test_a_third_player_joins_creates_a_new_game(self):
        game = set_up_test_game(num_players=2)

        player_c_c = Client()

        data = {
            "player_name": "Billy"
        }

        resp = player_c_c.post('/connect_five/connect', data=data)

        self.assertEqual(resp.status_code, 201)

        self.assertNotEqual(resp.json()["game"]["id"], game.id)

    def test_player_a_disconnects_from_game_sets_winner_player_b(self):

        game = set_up_test_game(num_players=2)

        player_a_c = Client()

        data = {
            "player_id": game.player_a.id,
            "game_id": game.id
        }

        resp = player_a_c.post('/connect_five/disconnect', data=data)

        self.assertEqual(resp.status_code, 201)

        self.assertEqual(resp.json()["game"]["winner"], game.player_b.id)

    def test_player_a_get_game(self):

        game = set_up_test_game(num_players=2)

        player_a_c = Client()

        resp = player_a_c.get('/connect_five/game/{}/'.format(str(game.id)))

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.json()["id"], str(game.id))

    def test_player_a_makes_a_move(self):

        game = set_up_test_game()
        player_a = game.player_a
        player_b = game.player_b
        game.current_turn = player_a
        game.save()

        player_a_client = Client()

        data = {
            "player_id": player_a.id,
            "game_id": game.id,
            "column": 1
        }

        resp = player_a_client.post('/connect_five/move', data)

        self.assertEqual(resp.status_code, 201)

        game_data = resp.json()["game"]

        board = json.loads(GameSession.objects.get(id=game.id).board)

        self.assertEqual(game_data["current_turn"], player_b.id)

        self.assertEqual(board[len(board)-1][0], [player_a.id])

    def test_player_a_horizontal_win(self):

        game = set_up_test_game()
        player_a = game.player_a
        game.current_turn = player_a
        game.save()

        player_a_client = Client()
        resp = None
        for i in range(5):

            data = {
                "player_id": player_a.id,
                "game_id": game.id,
                "column": i + 1
            }

            resp = player_a_client.post('/connect_five/move', data)

        game_data = resp.json()["game"]

        self.assertEqual(resp.json()["status"], "winner")
        self.assertEqual(game_data["winner"], player_a.id)

    def test_player_a_vertical_win(self):

        game = set_up_test_game()
        player_a = game.player_a
        game.current_turn = player_a
        game.save()

        player_a_client = Client()
        resp = None
        for i in range(5):
            data = {
                "player_id": player_a.id,
                "game_id": game.id,
                "column": 8
            }

            resp = player_a_client.post('/connect_five/move', data)

        game_data = resp.json()["game"]

        self.assertEqual(game_data["winner"], player_a.id)

    def test_player_a_diagonal_win_right_to_left(self):

        game = set_up_test_game()
        player_a = game.player_a
        player_b = game.player_b
        game.current_turn = player_a
        game.save()

        player_a_client = Client()
        player_b_client = Client()

        resp = None

        for i in range(0, 6):

            if i < 5:
                for y in range(i):
                    p_b_data = {
                        "player_id": player_b.id,
                        "game_id": game.id,
                        "column": i + 1
                    }

                    player_b_client.post('/connect_five/move', p_b_data)

            p_a_data = {
                "player_id": player_a.id,
                "game_id": game.id,
                "column": i
            }
            resp = player_a_client.post('/connect_five/move', p_a_data)

        game_data = resp.json()["game"]

        self.assertEqual(game_data["winner"], player_a.id)

    def test_player_a_diagonal_win_left_to_right(self):

        game = set_up_test_game()
        player_a = game.player_a
        player_b = game.player_b
        game.current_turn = player_a
        game.save()

        player_a_client = Client()
        player_b_client = Client()

        resp = None

        for i in range(0, 5):

            if i < 5:
                for y in range(i):
                    p_b_data = {
                        "player_id": player_b.id,
                        "game_id": game.id,
                        "column": 5 - i
                    }

                    player_b_client.post('/connect_five/move', p_b_data)

            p_a_data = {
                "player_id": player_a.id,
                "game_id": game.id,
                "column": 5 - i
            }
            resp = player_a_client.post('/connect_five/move', p_a_data)

        game_data = resp.json()["game"]

        self.assertEqual(game_data["winner"], player_a.id)

