import json
from .models import Player, GameSession, WIN_CONNECT
from .serializers import GameSerializer


def find_setup_game(player_name):

    new_player = Player(name=player_name)

    new_player.save()

    the_game = GameSession.objects.filter(
        player_b=None,
        winner=None
    ).first()

    if not the_game:
        the_game = GameSession.objects.create(
            player_a=new_player
        )
    else:
        the_game.player_b = new_player
        the_game.current_turn = the_game.player_a
        the_game.save()

    return {
            "status": "connected",
            "message": "You have joined the game.",
            "player_id": new_player.id,
            "game": GameSerializer(the_game).data
    }


def disconnect_game(player_id, game_id):

    game = GameSession.objects.get(pk=game_id)
    this_player = Player.objects.get(id=player_id)

    player_a = game.player_a
    player_b = game.player_b

    if player_a == this_player:
        winner = player_b
    else:
        winner = player_a

    game.winner = winner

    game.save()

    return {
        "status": "disconnected",
        "message": "You have disconnected from the game. You loose.",
        "player_id": this_player.id,
        "game": GameSerializer(game).data
    }


def game_move(player_id, game_id, column):
    """
    Makes a game move.
    :param player_id:
    :param game_id:
    :param column:
    :return:
    """

    game = GameSession.objects.get(pk=game_id)
    this_player = Player.objects.get(id=player_id)
    column -= 1  # zero indexed.

    row, col, msg = make_move(game, this_player.id, column)

    if not all([col > -1, row > -1]):
        return {
            "status": "bad_move",
            "message": "You have made an invalid move. {}".format(msg),
            "game": GameSerializer(game).data
        }

    is_win, win_msg = check_win(game, this_player, col, row)

    if is_win:
        game.winner = this_player
    else:
        opponent_player = game.player_a if this_player.id != game.player_a.id else game.player_b
        game.current_turn = opponent_player

    game.save()

    status = "winner" if is_win else "valid_move"

    return {
        "status": status,
        "message": win_msg if is_win else msg,
        "player_id": this_player.id,
        "game": GameSerializer(game).data
    }


def make_move(game, player_id, column):
    """

    :param game:
    :param player_id:
    :param column:
    :return:
    """

    board = json.loads(game.board)

    width = len(board[0])

    if not 0 <= column <= width - 1:
        return -1, -1, "Invalid column ({}), valid columns ({}-{})".format(column + 1, '1', str(width))

    for i, r in reversed(list(enumerate(board))):   # check from the bottom of the game board
        if not r[column]:
            r[column] = [player_id]
            game.board = json.dumps(board)
            game.save()
            return i, column, "Valid move made to (row {}, col, {})".format(width - i, column + 1)

    return -1, -1, "Did not make move. Check the column is not full."


def check_win(game, player, col, row):

    board = json.loads(game.board)

    is_win, msg = check_horizontals(board, player, row)

    if not is_win:
        is_win, msg = check_verticals(board, player, col)

    if not is_win:
        is_win, msg = check_diagonals(board, player, row, col)

    return is_win, msg


def check_verticals(board, player, col):
    """
    Checks for a win in the verticals
    :param board:
    :param player:
    :param col:
    :return:
    """
    count = 0
    height = len(board)

    for i in range(height):
        if board[i][col] == [player.id]:
            count += 1
        else:
            count = 0
        if count >= WIN_CONNECT:
            return True, "Player ({}) wins vertically.".format(player.name)
    return False, "Player did not win vertically: count ({})".format(count)


def check_horizontals(board, player, row):
    """
    Checks for win in horizontals.
    :param board:
    :param player:
    :param row:
    :return:
    """
    count = 0

    width = len(board[0])

    for i in range(width):
        if board[row][i] == [player.id]:
            count += 1
        else:
            count = 0
        if count >= WIN_CONNECT:
            return True, "Player ({}) wins! horizontally".format(player.name)
    return False, ""


def check_diagonals(board, player, row, col):
    """
    Checks for win in diagonals
    :param board:
    :param player:
    :param row:
    :param col:
    :return:
    """

    height = len(board)
    width = len(board[0])

    def _check_diagonally(c_dir="+", r_dir="+"):
        found_count = 0
        next_row = row + 1 if r_dir == "+" else row - 1
        next_col = col + 1 if c_dir == "+" else col - 1
        while _is_not_boundary(next_row, next_col, height, width, c_dir, r_dir):
            if board[next_row][next_col] == [player.id]:
                found_count += 1
                next_row = next_row + 1 if r_dir == "+" else next_row - 1
                next_col = next_col + 1 if c_dir == "+" else next_col - 1
            elif _is_not_boundary(next_row, next_col, height, width, c_dir, r_dir):
                break
        return found_count

    count = 1
    # Check from left to right diagonally
    if row != height - 1 and col != width - 1:
        count += _check_diagonally(c_dir="+", r_dir="+")
    if row != 0 and col != 0 and count < WIN_CONNECT:
        count += _check_diagonally(c_dir="-", r_dir="-")
    if count >= WIN_CONNECT:
        return True, "Player ({}) wins! diagonally (left to right)".format(player.name)

    count = 1
    # Check from right to left diagonally
    if row != height - 1 and col != 0:
        # Check
        count += _check_diagonally(c_dir="-", r_dir="+")
    if row != 0 and col != width - 1 and count < WIN_CONNECT:
        count += _check_diagonally(c_dir="+", r_dir="-")

    if count >= WIN_CONNECT:
        return True, "Player ({}) wins! diagonally (right to left).".format(player.name)

    return False, ""


def _is_not_boundary(next_row, next_col, height, width, c_dir="+", r_dir="+"):
    """
    Checks if a boundary was hit
    :param next_row:
    :param next_col:
    :param height:
    :param width:
    :param c_dir:
    :param r_dir:
    :return:
    """

    if c_dir == "+" and r_dir != "+":
        # Moving up left to right
        return (next_col < width) and (next_row >= 0)
    elif c_dir != "+" and r_dir == "+":
        # Moving down and right to left
        return (next_col >= 0) and (next_row < height)
    elif c_dir != "+" and r_dir != "+":
        # Moving up and right to left
        return (next_col >= 0) and (next_row >= 0)
    else:  # c_dir == "+" and r_dir == "+"
        # Moving down and left to right
        return (next_col < width) and (next_row < height)
