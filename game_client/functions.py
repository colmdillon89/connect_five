from os import system, name, path
import json
from time import sleep
import requests


class GameClient:

    POLL_TIMER = 2

    def __init__(self, player_name, hostname, game_piece):
        self.player_name = player_name
        self.hostname = hostname
        self.game_piece = game_piece
        self.game_id = None
        self.player_id = 0
        self.opponent_id = 0
        self.BASE_URL = 'http://' + self.hostname + '/connect_five'
        self.CONNECT_URL = path.join(self.BASE_URL, 'connect')
        self.DISCONNECT_URL = path.join(self.BASE_URL, 'disconnect')
        self.GET_GAME_URL = path.join(self.BASE_URL, 'game')
        self.MAKE_MOVE = path.join(self.BASE_URL, 'move')

    def create_join_game(self):
        """
        Join or create new game.
        :return:
        """

        print("Connecting to next available game @ {}".format(self.CONNECT_URL))

        resp = requests.post(
            url=self.CONNECT_URL,
            data={
                "player_name": self.player_name
            }
        )

        resp.raise_for_status()

        game_data = resp.json()["game"]

        self.game_id = game_data["id"]
        self.player_id = resp.json()["player_id"]

        print("Player ID = {}".format(self.player_id))
        print("Game ID = {}".format(self.game_id))

        self._connect()

    def _connect(self):
        """
        Connect to the game. Runs the game loop.
        :return:
        """

        try:
            while True:

                self.clear()

                game_state = self._get_game_state()

                self._display_game(json.loads(game_state["board"]))
                
                if game_state["player_a"] == self.player_id and game_state["player_b"]:
                    self.opponent_id = game_state["player_b"]
                elif game_state["player_b"] == self.player_id:
                    self.opponent_id = game_state["player_a"]
                else:
                    print("Waiting for an opponent")

                if self.player_id and self.opponent_id:
                    if game_state["winner"]:

                        if game_state["winner"] == self.player_id:
                            print("Congratulations you won!")

                        else:
                            print("Sorry {} you lost the game!".format(self.player_name))

                        exit(0)

                    if game_state["current_turn"] == self.player_id:

                        col = input("Its your turn to make a move. Choose a column (1, 9), Your piece is ({}): "
                                    "".format(self.game_piece))
                        try:
                            int(col)
                        except ValueError:
                            continue

                        data = self.send_move(col)

                        if len(data["message"]) > 0:
                            print(data["message"])

                        if data["status"] in ["winner", "disconnected"]:
                            exit(0)
                        elif data["status"] in ["bad_move"]:
                            continue
                    else:
                        print("Waiting for your opponents move..")

                sleep(self.POLL_TIMER)

        except (KeyboardInterrupt, Exception):
            resp = self.disconnect()
            print(resp["message"])
            exit(0)

    def _display_game(self, board):
        """
        Displays the game board
        :param board:
        :return:
        """
        opp_game_piece = "X" if self.game_piece == "O" else "O"
        for r in board:
            r_str = ""
            for c in r:
                if c:
                    if c == [self.player_id]:
                        r_str += "[{}]".format(self.game_piece)
                    else:
                        r_str += "[{}]".format(opp_game_piece)
                else:
                    r_str += "[ ]"
            print(r_str + "\n")

    def send_move(self, col):
        """
        Sends a move to the game.
        :param col:
        :return:
        """
        resp = requests.post(
            url=self.MAKE_MOVE,
            data={
                "player_id": self.player_id,
                "game_id": self.game_id,
                "column": col
            }
        )

        resp.raise_for_status()

        return resp.json()

    def _get_game_state(self):
        """
        Gets current game state for this game.
        :return:
        """
        url = self.GET_GAME_URL + "/{}".format(self.game_id)

        resp = requests.get(
            url=url
        )

        resp.raise_for_status()

        return resp.json()

    def disconnect(self):
        """
        Disconnects from the game.
        :return:
        """

        resp = requests.post(
            url=self.DISCONNECT_URL,
            data={
                "player_id": self.player_id,
                "game_id": self.game_id
            }
        )

        resp.raise_for_status()

        return resp.json()

    @staticmethod
    def clear():
        """
        Clears the window - aesthetics.
        :return:
        """

        # for windows
        if name == 'nt':
            _ = system('cls')

            # for mac and linux(here, os.name is 'posix')
        else:
            _ = system('clear')

