#### Connect Five
###### Developer: Colm Dillon

This connect five game is build with python 3.7 and Django REST framework using a server client architecture.

##### Setup

- create a blank dir
    - ``mkdir genesys``
    - ``cd genesys``

- create a virtual environment
    - ``python3 -m venv venv``
    - ``. venv/bin/activate``

- clone repo 
    - ``git clone git@gitlab.com:colmdillon89/connect_five.git``

- install required python packages
    - ``pip install --upgrade pip``
    - ``pip install -r connect_five/requirements.txt``
    
- make migrations to sqlite database
    - ``cd connect_five``
    - ``python manage.py migrate``
    
- run unit tests
    - ``python manage.py test``

- run the application server
    - ``python manage.py runserver 0.0.0.0:8888``
    - Choose another port if :8888 not available
    - You should see something similar to below 
    ```
    Watching for file changes with StatReloader
    Performing system checks...
    
    System check identified no issues (0 silenced).
    November 12, 2019 - 13:59:05
    Django version 2.2.7, using settings 'connect_five.settings'
    Starting development server at http://0.0.0.0:8888/
    Quit the server with CONTROL-C.
    ```
  
- create two game clients
   - Open two terminals 
   - change directory to where you cloned the repo.
   - ``. venv/bin/activate``
   - ``cd connect_five``
   - Run a game client in each terminal
   - ``
      python game_client.py <player_name> -g 0.0.0.0:8888
     ``

##### Game logic and assumptions

 1. The first player will wait for another player to join.
 2. Each player will take turns dropping a piece into a column (1-9)
 3. Player can win connecting five pieces vertically, horizontally and diagonally.
 4. If a player disconnects with Ctrl+C the opposing player wins the game.
 5. If a third player tries to join they will create a new game and wait 
 for a fourth player.
 
 
 
##### Some improvements for the future.
Due to the time constraints I have some improvements I would work on with more time.
 1. Run the application in a Docker container to avoid the setup above.
 2. I would like to create more integration tests for the client.
 3. I would prob refactor some of the client code. Break out some of
 the logic from inside the While loop, etc. 
 