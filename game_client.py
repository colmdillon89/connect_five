# The connect five game client
import argparse

from game_client.functions import GameClient


def main():

    arg_parser = argparse.ArgumentParser(description="The connect 5 game client ..")

    arg_parser.add_argument(
        'player_name',
        type=str,
        help='Player name'
    )

    arg_parser.add_argument(
        "--game_piece",
        "-p",
        help="Game piece X or O",
        default="X"
    )

    arg_parser.add_argument(
        '--game_host',
        '-g',
        type=str,
        help='url to the connect 5 server',
        default='localhost:8000'
    )

    args = arg_parser.parse_args()

    print("{}! Welcome to Connect5".format(args.player_name))

    game_client = GameClient(
        player_name=args.player_name,
        hostname=args.game_host,
        game_piece=args.game_piece
    )

    game_client.create_join_game()


if __name__ == '__main__':
    main()
